using System;
using Unity.Mathematics;

namespace Monoid.Unity.Mathematics {

  #region circle

  [Serializable]
  public struct circle {
    public float2 center;
    public float radius;
  }

  #endregion

  #region extremes2

  [Serializable]
  public struct extremes2 {
    public float2 min, max;

    public float2 At(float2 t) => math.lerp(min, max, t);

    public static explicit operator extremes2(in aabox2 b) => new extremes2 { min = b.center - b.extents, max = b.center + b.extents };
  }

  #endregion

  #region aabox2

  [Serializable]
  public struct aabox2 {
    public float2 center, extents;

    public static explicit operator box2(in aabox2 b) => new box2 { center = b.center, extents = b.extents };
  }

  #endregion

  #region box2

  [Serializable]
  public struct box2 {
    public float2 center, extents;
    public float rotation;

    public static explicit operator aabox2(in box2 b) => new aabox2 { center = b.center, extents = b.extents };
  }

  #endregion

  #region line2

  [Serializable]
  public struct line2 {
    public float2 from, to;

    unsafe public ref float2 this[int index] {
      get {
#if ENABLE_UNITY_COLLECTIONS_CHECKS
        if ((uint)index >= 2) {
          throw new System.ArgumentException("index must be between[0...1]");
        }
#endif
        fixed (float2* array = &from) { return ref array[index]; }
      }
    }

    public float2 At(float t) => math.lerp(from, to, t);
  }

  #endregion
}
