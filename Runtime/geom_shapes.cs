﻿using System.Runtime.CompilerServices;
using Unity.Mathematics;

namespace Monoid.Unity.Mathematics {

  unsafe public static partial class geom {


    // axes -------------------------------------------------------------------

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    static float4 axes4(float sin, float cos) => math.float4(cos, sin, -sin, cos);


    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    static float2x2 axes2x2(float sin, float cos) => math.float2x2(math.float2(cos, sin), math.float2(-sin, cos));


    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    static float4 axes4(float angle) {
      math.sincos(angle, out var sin, out var cos);
      return axes4(sin, cos);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float2x2 axes2x2(float angle) {
      math.sincos(angle, out var sin, out var cos);
      return axes2x2(sin, cos);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    static float4 axes4() => math.float4(1, 0, 0, 1);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float2x2 axes2x2() => math.float2x2(math.float2(1, 0), math.float2(0, 1));


    // extended axes ----------------------------------------------------------

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    static float4 eaxes4(float sin, float cos, float2 extents) => axes4(sin, cos) * extents.xxyy;

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    static float2x2 eaxes2x2(float sin, float cos, float2 extents) {
      var axes = eaxes4(sin, cos, extents);
      return math.float2x2(axes.xy, axes.zw);
    }


    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    static float4 eaxes4(float angle, float2 extents) => axes4(angle) * extents.xxyy;

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float2x2 eaxes2x2(float angle, float2 extents) {
      var axes = eaxes4(angle, extents);
      return math.float2x2(axes.xy, axes.zw);
    }


    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float2x2 eaxes2x2(float2 extents) {
      var axes = eaxes4(extents);
      return math.float2x2(axes.xy, axes.zw);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    static float4 eaxes4(float2 extents) => axes4() * extents.xxyy;


    // box corners ------------------------------------------------------------

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    unsafe static void boxcorners(float2 center, float2 axis1, float2 axis2, float2* corners) {
      float2 l = center - axis1;
      float2 r = center + axis1;
      corners[0] = r + axis2;
      corners[1] = l + axis2;
      corners[2] = l - axis2;
      corners[3] = r - axis2;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    unsafe static void boxcorners(float2 center, float2 axis1, float2 axis2, float2[] corners, int start) {
      float2 l = center - axis1;
      float2 r = center + axis1;
      corners[start+0] = r + axis2;
      corners[start+1] = l + axis2;
      corners[start+2] = l - axis2;
      corners[start+3] = r - axis2;
    }


    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public unsafe static void boxcorners(in aabox2 b, float2* corners) {
      float2x2 axes = eaxes2x2(b.extents);
      boxcorners(b.center, axes[0], axes[1], corners);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public unsafe static void boxcorners(in aabox2 b, float2[] corners, int start=0) {
      float2x2 axes = eaxes2x2(b.extents);
      boxcorners(b.center, axes[0], axes[1], corners, start);
    }


    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public unsafe static void boxcorners(in box2 b, float2* corners) {
      float2x2 axes = eaxes2x2(b.rotation, b.extents);
      boxcorners(b.center, axes[0], axes[1], corners);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public unsafe static void boxcorners(in box2 b, float2[] corners, int start=0) {
      float2x2 axes = eaxes2x2(b.rotation, b.extents);
      boxcorners(b.center, axes[0], axes[1], corners, start);
    }

  }

}
