using System;
using System.Runtime.CompilerServices;
using Unity.Mathematics;

namespace Monoid.Unity.Mathematics {

  unsafe public static partial class geom {

    /// no segment intersection iff both times are either < 0 or > 1
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool segment(float2 t) => !(math.all(t < 0) | math.all(t > 1));

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool segment(float2 t, bool hit) => hit & segment(t);


    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool cast(in line2 line, float2 extents, out float2 t) {
      float2 dir = line.to - line.from;

      float2 t0 = (-extents - line.from) / dir;
      float2 t1 = (+extents - line.from) / dir;

      t = math.float2(
        math.cmax(math.min(t0, t1)),
        math.cmin(math.max(t0, t1))
      );

      return t.x <= t.y;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool cast(in line2 line, in aabox2 box, out float2 t) {
      var l = new line2 {
        from = line.from - box.center,
        to = line.to - box.center,
      };
      return cast(in l, box.extents, out t);
  	}

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool cast(in line2 line, in box2 box, out float2 t) {
      math.sincos(-box.rotation, out float sin, out float cos);
      var l = new line2 {
        from = rotate(line.from - box.center, sin, cos),
        to = rotate(line.to - box.center, sin, cos),
      };
      return cast(in l, box.extents, out t);
  	}


    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool cast(in line2 line, in circle circle, out float2 t) {
      // http: //stackoverflow.com/questions/1073336/circle-line-segment-collision-detection-algorithm

      float2 dir = line.to - line.from;
      float2 off = circle.center - line.from;

      float a = math.lengthsq(dir);
      float b = 2 * math.dot(off, dir);
      float c = math.lengthsq(off) - circle.radius*circle.radius;

      float d = b*b - 4*a*c;
      bool hit = d >= 0;
      d = math.sqrt(d);

      t = math.float2(b - d, b + d);
      if (a != 0) {
        t /= 2*a;
      }
      t = math.select(t, t.yx, t.x > t.y); // swap (hopefully BURST gets rid of the dynamic branching)

      return hit;
    }


    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool intersect(in line2 a, in line2 b, out float2 t) {
      float2 dA = a.to - a.from;
      float2 dB = b.to - b.from;

      float y = cross(dA, dB);
      float2 x = a.from - b.from;
      t = math.float2(cross(dB, x), cross(dA, x)) / y;
      return y != 0;
    }


    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    static float cross(float2 a, float2 b) => a.x*b.y - b.x*a.y;


    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    static float2 rotate(float2 v, float sin, float cos) => math.float2(cos*v.x - sin*v.y, sin*v.x + cos*v.y);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    static float2 rotate(float2 v, float angle) {
      math.sincos(angle, out float s, out float c);
      return rotate(v, s, c);
    }
  }
}
