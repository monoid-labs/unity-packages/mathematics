using System.Runtime.CompilerServices;
using Unity.Mathematics;

namespace Monoid.Unity.Mathematics {

  unsafe public static partial class geom {

    // overlap ----------------------------------------------------------------

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool overlap(in circle c1, in circle c2) {
      // return sdistance(c1, c2) <= 0;
    	var a = math.lengthsq(c1.center - c2.center);
	    var b = c1.radius + c2.radius;
	    return a <= b*b;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool overlap(in circle c, in aabox2 b) => distance(b, c.center) <= c.radius;


    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool overlap(in aabox2 a, in aabox2 b) {
      return math.all(math.abs(a.center - b.center) <= (a.extents + b.extents));
    }

    public static bool overlap(in box2 a, in box2 b) {
      var axes_a = eaxes2x2(a.rotation, a.extents);
      var axes_b = eaxes2x2(b.rotation, b.extents);
      var corners_a = stackalloc float2[4];
      var corners_b = stackalloc float2[4];

      boxcorners(a.center, axes_a[0], axes_a[1], corners_a);
      boxcorners(b.center, axes_b[0], axes_b[1], corners_b);
      return overlap(axes_a[0], corners_a, 4, corners_b, 4) & overlap(axes_a[1], corners_a, 4, corners_b, 4) &
             overlap(axes_b[0], corners_a, 4, corners_b, 4) & overlap(axes_b[1], corners_a, 4, corners_b, 4);
    }

    // seperating axes overlap ------------------------------------------------

    static bool overlap(float2 axis, float2* a, int m, float2* b, int n) {
      var minmax_a = sepaxes_minmax(axis, a, m);
      var minmax_b = sepaxes_minmax(axis, b, n);
      return (minmax_a.x <= minmax_b.x & minmax_b.x <= minmax_a.y) | (minmax_b.x <= minmax_a.x & minmax_a.x <= minmax_b.y);
    }

    static float2 sepaxes_minmax(float2 axis, float2* points, int n) {
      var minmax = math.float2(float.PositiveInfinity, float.NegativeInfinity);
      for (int i = 0; i < n; ++i) {
        var t = math.dot(axis, points[i]);
        minmax.x = math.min(minmax.x, t);
        minmax.y = math.max(t, minmax.y);
      }
      return minmax;
    }

  }

}
