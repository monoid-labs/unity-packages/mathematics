using System.Runtime.CompilerServices;
using Unity.Mathematics;

namespace Monoid.Unity.Mathematics {

  unsafe public static partial class geom {

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static extremes2 bounds(in circle c) => new extremes2{min = c.center - c.radius, max = c.center + c.radius};

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static extremes2 bounds(in aabox2 b) => new extremes2{min = b.center - b.extents, max = b.center + b.extents};

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static extremes2 bounds(in box2 b) {
      var corners = stackalloc float2[4];
      boxcorners(in b, corners);
      return new extremes2{
        min = math.min(math.min(corners[0], corners[1]),math.min(corners[2], corners[3])),
        max = math.max(math.max(corners[0], corners[1]),math.max(corners[2], corners[3])),
      };
    }


  }
}