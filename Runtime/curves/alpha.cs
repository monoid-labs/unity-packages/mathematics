using System.Runtime.CompilerServices; // mscorlib.dll
using Unity.Mathematics;

namespace Monoid.Unity.Mathematics {

  public static partial class alpha {

    #region steady (linear): bezier.linear(A,B, t) | bezier.linear(0,1, t)

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float steady(float t) => bezier.linear(0, 1, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float steady(float a, float b, float t) => bezier.linear(a, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float2 steady(float2 a, float2 b, float t) => bezier.linear(a, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float2 steady(float2 a, float2 b, float2 t) => bezier.linear(a, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float3 steady(float3 a, float3 b, float t) => bezier.linear(a, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float3 steady(float3 a, float3 b, float3 t) => bezier.linear(a, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float4 steady(float4 a, float4 b, float t) => bezier.linear(a, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float4 steady(float4 a, float4 b, float4 t) => bezier.linear(a, b, t);

    #endregion


    #region smoothstart2 (quadric): bezier.quadric(A,A,B, t) | bezier.quadric(0,0,1, t)

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float smoothstart2(float t) => smoothstart2(0, 1, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float smoothstart2(float a, float b, float t) => bezier.quadric(a, a, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float2 smoothstart2(float2 a, float2 b, float t) => bezier.quadric(a, a, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float2 smoothstart2(float2 a, float2 b, float2 t) => bezier.quadric(a, a, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float3 smoothstart2(float3 a, float3 b, float t) => bezier.quadric(a, a, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float3 smoothstart2(float3 a, float3 b, float3 t) => bezier.quadric(a, a, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float4 smoothstart2(float4 a, float4 b, float t) => bezier.quadric(a, a, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float4 smoothstart2(float4 a, float4 b, float4 t) => bezier.quadric(a, a, b, t);

    #endregion

    #region smoothstop2 (quadric): bezier.quadric(A,B,B, t) | bezier.quadric(0,1,1, t)

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float smoothstop2(float t) => smoothstop2(0, 1, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float smoothstop2(float a, float b, float t) => bezier.quadric(a, b, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float2 smoothstop2(float2 a, float2 b, float t) => bezier.quadric(a, b, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float2 smoothstop2(float2 a, float2 b, float2 t) => bezier.quadric(a, b, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float3 smoothstop2(float3 a, float3 b, float t) => bezier.quadric(a, b, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float3 smoothstop2(float3 a, float3 b, float3 t) => bezier.quadric(a, b, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float4 smoothstop2(float4 a, float4 b, float t) => bezier.quadric(a, b, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float4 smoothstop2(float4 a, float4 b, float4 t) => bezier.quadric(a, b, b, t);

    #endregion


    #region smoothstep3 (quadric): bezier.cubic(A,A,B,B, t) | bezier.cubic(0,0,1,1, t) | bezier.linear(smoothstart2(A,B, t), smoothstop2(A,B, t), t)

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float smoothstep3(float t) => smoothstep3(0, 1, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float smoothstep3(float a, float b, float t) => bezier.cubic(a, a, b, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float2 smoothstep3(float2 a, float2 b, float t) => bezier.cubic(a, a, b, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float2 smoothstep3(float2 a, float2 b, float2 t) => bezier.cubic(a, a, b, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float3 smoothstep3(float3 a, float3 b, float t) => bezier.cubic(a, a, b, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float3 smoothstep3(float3 a, float3 b, float3 t) => bezier.cubic(a, a, b, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float4 smoothstep3(float4 a, float4 b, float t) => bezier.cubic(a, a, b, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float4 smoothstep3(float4 a, float4 b, float4 t) => bezier.cubic(a, a, b, b, t);

    #endregion

    #region smoothstart3 (quadric): bezier.cubic(A,A,A,B, t) | bezier.cubic(0,0,0,1, t)

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float smoothstart3(float t) => smoothstart3(0, 1, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float smoothstart3(float a, float b, float t) => bezier.cubic(a, a, a, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float2 smoothstart3(float2 a, float2 b, float t) => bezier.cubic(a, a, a, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float2 smoothstart3(float2 a, float2 b, float2 t) => bezier.cubic(a, a, a, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float3 smoothstart3(float3 a, float3 b, float t) => bezier.cubic(a, a, a, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float3 smoothstart3(float3 a, float3 b, float3 t) => bezier.cubic(a, a, a, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float4 smoothstart3(float4 a, float4 b, float t) => bezier.cubic(a, a, a, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float4 smoothstart3(float4 a, float4 b, float4 t) => bezier.cubic(a, a, a, b, t);

    #endregion

    #region smoothstop3 (quadric): bezier.cubic(A,B,B,B, t) | bezier.cubic(0,1,1,1, t)

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float smoothstop3(float t) => smoothstop3(0, 1, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float smoothstop3(float a, float b, float t) => bezier.cubic(a, b, b, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float2 smoothstop3(float2 a, float2 b, float t) => bezier.cubic(a, b, b, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float2 smoothstop3(float2 a, float2 b, float2 t) => bezier.cubic(a, b, b, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float3 smoothstop3(float3 a, float3 b, float t) => bezier.cubic(a, b, b, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float3 smoothstop3(float3 a, float3 b, float3 t) => bezier.cubic(a, b, b, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float4 smoothstop3(float4 a, float4 b, float t) => bezier.cubic(a, b, b, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float4 smoothstop3(float4 a, float4 b, float4 t) => bezier.cubic(a, b, b, b, t);

    #endregion

    #region hesitate3 (quadric): bezier.cubic(A,B,A,B, t) | bezier.cubic(0,1,0,1, t) | bezier.linear(smoothstop2(A,B, t), smoothstart2(A,B, t), t)

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float hesitate3(float t) => hesitate3(0, 1, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float hesitate3(float a, float b, float t) => bezier.cubic(a, b, a, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float2 hesitate3(float2 a, float2 b, float t) => bezier.cubic(a, b, a, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float2 hesitate3(float2 a, float2 b, float2 t) => bezier.cubic(a, b, a, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float3 hesitate3(float3 a, float3 b, float t) => bezier.cubic(a, b, a, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float3 hesitate3(float3 a, float3 b, float3 t) => bezier.cubic(a, b, a, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float4 hesitate3(float4 a, float4 b, float t) => bezier.cubic(a, b, a, b, t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float4 hesitate3(float4 a, float4 b, float4 t) => bezier.cubic(a, b, a, b, t);

    #endregion


    #region integral

    public static partial class integral {

      #region lerp (linear): bezier.linear(A,B, t) | bezier.linear(0,1, t)

      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float lerp(float t) => lerp(0, 1, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float lerp(float a, float b, float t) => bezier.integral.linear(a, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float2 lerp(float2 a, float2 b, float t) => bezier.integral.linear(a, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float2 lerp(float2 a, float2 b, float2 t) => bezier.integral.linear(a, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float3 lerp(float3 a, float3 b, float t) => bezier.integral.linear(a, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float3 lerp(float3 a, float3 b, float3 t) => bezier.integral.linear(a, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float4 lerp(float4 a, float4 b, float t) => bezier.integral.linear(a, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float4 lerp(float4 a, float4 b, float4 t) => bezier.integral.linear(a, b, t);

      #endregion

      #region smoothstart2 (quadric): bezier.quadric(A,A,B, t) | bezier.quadric(0,0,1, t)

      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float smoothstart2(float t) => smoothstart2(0, 1, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float smoothstart2(float a, float b, float t) => bezier.integral.quadric(a, a, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float2 smoothstart2(float2 a, float2 b, float t) => bezier.integral.quadric(a, a, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float2 smoothstart2(float2 a, float2 b, float2 t) => bezier.integral.quadric(a, a, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float3 smoothstart2(float3 a, float3 b, float t) => bezier.integral.quadric(a, a, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float3 smoothstart2(float3 a, float3 b, float3 t) => bezier.integral.quadric(a, a, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float4 smoothstart2(float4 a, float4 b, float t) => bezier.integral.quadric(a, a, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float4 smoothstart2(float4 a, float4 b, float4 t) => bezier.integral.quadric(a, a, b, t);

      #endregion

      #region smoothstop2 (quadric): bezier.quadric(A,B,B, t) | bezier.quadric(0,1,1, t)

      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float smoothstop2(float t) => smoothstop2(0, 1, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float smoothstop2(float a, float b, float t) => bezier.integral.quadric(a, b, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float2 smoothstop2(float2 a, float2 b, float t) => bezier.integral.quadric(a, b, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float2 smoothstop2(float2 a, float2 b, float2 t) => bezier.integral.quadric(a, b, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float3 smoothstop2(float3 a, float3 b, float t) => bezier.integral.quadric(a, b, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float3 smoothstop2(float3 a, float3 b, float3 t) => bezier.integral.quadric(a, b, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float4 smoothstop2(float4 a, float4 b, float t) => bezier.integral.quadric(a, b, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float4 smoothstop2(float4 a, float4 b, float4 t) => bezier.integral.quadric(a, b, b, t);

      #endregion


      #region smoothstep3 (quadric): bezier.cubic(A,A,B,B, t) | bezier.cubic(0,0,1,1, t) | bezier.linear(smoothstart2(A,B, t), smoothstop2(A,B, t), t)

      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float smoothstep3(float t) => smoothstep3(0, 1, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float smoothstep3(float a, float b, float t) => bezier.integral.cubic(a, a, b, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float2 smoothstep3(float2 a, float2 b, float t) => bezier.integral.cubic(a, a, b, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float2 smoothstep3(float2 a, float2 b, float2 t) => bezier.integral.cubic(a, a, b, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float3 smoothstep3(float3 a, float3 b, float t) => bezier.integral.cubic(a, a, b, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float3 smoothstep3(float3 a, float3 b, float3 t) => bezier.integral.cubic(a, a, b, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float4 smoothstep3(float4 a, float4 b, float t) => bezier.integral.cubic(a, a, b, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float4 smoothstep3(float4 a, float4 b, float4 t) => bezier.integral.cubic(a, a, b, b, t);

      #endregion

      #region smoothstart3 (quadric): bezier.cubic(A,A,A,B, t) | bezier.cubic(0,0,0,1, t)

      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float smoothstart3(float t) => smoothstart3(0, 1, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float smoothstart3(float a, float b, float t) => bezier.integral.cubic(a, a, a, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float2 smoothstart3(float2 a, float2 b, float t) => bezier.integral.cubic(a, a, a, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float2 smoothstart3(float2 a, float2 b, float2 t) => bezier.integral.cubic(a, a, a, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float3 smoothstart3(float3 a, float3 b, float t) => bezier.integral.cubic(a, a, a, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float3 smoothstart3(float3 a, float3 b, float3 t) => bezier.integral.cubic(a, a, a, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float4 smoothstart3(float4 a, float4 b, float t) => bezier.integral.cubic(a, a, a, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float4 smoothstart3(float4 a, float4 b, float4 t) => bezier.integral.cubic(a, a, a, b, t);

      #endregion

      #region smoothstop3 (quadric): bezier.cubic(A,B,B,B, t) | bezier.cubic(0,1,1,1, t)

      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float smoothstop3(float t) => smoothstop3(0, 1, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float smoothstop3(float a, float b, float t) => bezier.integral.cubic(a, b, b, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float2 smoothstop3(float2 a, float2 b, float t) => bezier.integral.cubic(a, b, b, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float2 smoothstop3(float2 a, float2 b, float2 t) => bezier.integral.cubic(a, b, b, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float3 smoothstop3(float3 a, float3 b, float t) => bezier.integral.cubic(a, b, b, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float3 smoothstop3(float3 a, float3 b, float3 t) => bezier.integral.cubic(a, b, b, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float4 smoothstop3(float4 a, float4 b, float t) => bezier.integral.cubic(a, b, b, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float4 smoothstop3(float4 a, float4 b, float4 t) => bezier.integral.cubic(a, b, b, b, t);

      #endregion

      #region hesitate3 (quadric): bezier.cubic(A,B,A,B, t) | bezier.cubic(0,1,0,1, t) | bezier.linear(smoothstop2(A,B, t), smoothstart2(A,B, t), t)

      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float hesitate3(float t) => hesitate3(0, 1, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float hesitate3(float a, float b, float t) => bezier.integral.cubic(a, b, a, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float2 hesitate3(float2 a, float2 b, float t) => bezier.integral.cubic(a, b, a, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float2 hesitate3(float2 a, float2 b, float2 t) => bezier.integral.cubic(a, b, a, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float3 hesitate3(float3 a, float3 b, float t) => bezier.integral.cubic(a, b, a, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float3 hesitate3(float3 a, float3 b, float3 t) => bezier.integral.cubic(a, b, a, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float4 hesitate3(float4 a, float4 b, float t) => bezier.integral.cubic(a, b, a, b, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float4 hesitate3(float4 a, float4 b, float4 t) => bezier.integral.cubic(a, b, a, b, t);

      #endregion

    }

    #endregion

  }

}
