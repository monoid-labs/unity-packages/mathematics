using System.Runtime.CompilerServices; // mscorlib.dll
using Unity.Mathematics;

namespace Monoid.Unity.Mathematics {

  public static partial class bezier {

    #region linear

    // A*s + B*t      | with s=1-t
    // A - A*t + B*t  | expanded         ( 2* 2+)
    // A + (B-A)*t    | grouped by t     ( 1* 2+)

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float linear(float a, float b, float t) => a + (b-a)*t;
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float2 linear(float2 a, float2 b, float t) => a + (b-a)*t;
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float2 linear(float2 a, float2 b, float2 t) => a + (b-a)*t;
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float3 linear(float3 a, float3 b, float t) => a + (b-a)*t;
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float3 linear(float3 a, float3 b, float3 t) => a + (b-a)*t;
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float4 linear(float4 a, float4 b, float t) => a + (b-a)*t;
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float4 linear(float4 a, float4 b, float4 t) => a + (b-a)*t;

    #endregion


    #region quadric

    // A*s^2 + 2*B*s*t + C*t^2                                      | with s=1-t
    // A*t^2 - 2*A*t + A - 2*B*t^2 + 2*B*t + C*t^2                  | expanded       (11*  5+)
    // A + (B-A)*t + ((B + (C-B)*t) - (A + (B-A)*t))*t              | grouped by t   ( 4*  8+)
    // quadric(A,B, t) = linear(linear(A,B, t), linear(B,C, t), t)  | re-use         ( 3*  6+)

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float quadric(float a, float b, float c, float t) => linear(linear(a, b, t), linear(b, c, t), t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float2 quadric(float2 a, float2 b, float2 c, float t) => linear(linear(a, b, t), linear(b, c, t), t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float2 quadric(float2 a, float2 b, float2 c, float2 t) => linear(linear(a, b, t), linear(b, c, t), t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float3 quadric(float3 a, float3 b, float3 c, float t) => linear(linear(a, b, t), linear(b, c, t), t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float3 quadric(float3 a, float3 b, float3 c, float3 t) => linear(linear(a, b, t), linear(b, c, t), t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float4 quadric(float4 a, float4 b, float4 c, float t) => linear(linear(a, b, t), linear(b, c, t), t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float4 quadric(float4 a, float4 b, float4 c, float4 t) => linear(linear(a, b, t), linear(b, c, t), t);

    #endregion


    #region cubic:

    // A*s^3 + 3*B*s^2*t + 3*C*s*t^2 + D*t^3                                                  | with s=1-t
    // -A*t^3 + 3*A*t^2 - 3*A*t + A + 3*B*t^3 - 6*B*t^2 + 3*B*t - 3*C*t^3 + 3*C*t^2 + D*t^3   | expanded    (27*  9+)
    // cubic(A,B,C, t) = linear(quadric(A,B,C, t), quadric(B,C,D, t), t)                      | re-use      ( 7* 14+)

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float cubic(float a, float b, float c, float d, float t) => linear(quadric(a, b, c, t), quadric(b, c, d, t), t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float2 cubic(float2 a, float2 b, float2 c, float2 d, float t) => linear(quadric(a, b, c, t), quadric(b, c, d, t), t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float2 cubic(float2 a, float2 b, float2 c, float2 d, float2 t) => linear(quadric(a, b, c, t), quadric(b, c, d, t), t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float3 cubic(float3 a, float3 b, float3 c, float3 d, float t) => linear(quadric(a, b, c, t), quadric(b, c, d, t), t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float3 cubic(float3 a, float3 b, float3 c, float3 d, float3 t) => linear(quadric(a, b, c, t), quadric(b, c, d, t), t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float4 cubic(float4 a, float4 b, float4 c, float4 d, float t) => linear(quadric(a, b, c, t), quadric(b, c, d, t), t);
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float4 cubic(float4 a, float4 b, float4 c, float4 d, float4 t) => linear(quadric(a, b, c, t), quadric(b, c, d, t), t);

    #endregion


    #region integral

    public static partial class integral {

      // integration by parts
      // ∫(t*(f(t)) = t*∫f(t) - ∫∫f(t)

      #region linear

      // ∫(A + (B-A)*t) = ∫(A) + ∫((B-A)*t) = A*t + (B-A)/2*t^2 = (A + (B-A)/2*t)*t
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float linear(float a, float b, float t) => (a + (b-a)/2*t)*t;
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float2 linear(float2 a, float2 b, float t) => (a + (b-a)/2*t)*t;
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float2 linear(float2 a, float2 b, float2 t) => (a + (b-a)/2*t)*t;
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float3 linear(float3 a, float3 b, float t) => (a + (b-a)/2*t)*t;
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float3 linear(float3 a, float3 b, float3 t) => (a + (b-a)/2*t)*t;
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float4 linear(float4 a, float4 b, float t) => (a + (b-a)/2*t)*t;
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float4 linear(float4 a, float4 b, float4 t) => (a + (b-a)/2*t)*t;


      // ∫(A*t + (B-A)/2*t^2) = ∫(A*t) + ∫(B-A)/2*t^2) = A/2*t^2 + (B-A)/6*t^3 = (A + (B-A)/3*t)/2*t^2
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float linear2(float a, float b, float t) => (a + (b-a)/3*t)/2*t*t;
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float2 linear2(float2 a, float2 b, float t) => (a + (b-a)/3*t)/2*t*t;
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float2 linear2(float2 a, float2 b, float2 t) => (a + (b-a)/3*t)/2*t*t;
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float3 linear2(float3 a, float3 b, float t) => (a + (b-a)/3*t)/2*t*t;
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float3 linear2(float3 a, float3 b, float3 t) => (a + (b-a)/3*t)/2*t*t;
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float4 linear2(float4 a, float4 b, float t) => (a + (b-a)/3*t)/2*t*t;
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float4 linear2(float4 a, float4 b, float4 t) => (a + (b-a)/3*t)/2*t*t;

      // ∫(A/2*t^2 + (B-A)/6*t^3) = ∫(A/2*t^2) + ∫((B-A)/6*t^3) = A/6*t^3 + (B-A)/24*t^4 = (A + (B-A)/4*t)/6*t^3
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float linear3(float a, float b, float t) => (a + (b-a)/4*t)/6*t*t*t;
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float2 linear3(float2 a, float2 b, float t) => (a + (b-a)/4*t)/6*t*t*t;
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float2 linear3(float2 a, float2 b, float2 t) => (a + (b-a)/4*t)/6*t*t*t;
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float3 linear3(float3 a, float3 b, float t) => (a + (b-a)/4*t)/6*t*t*t;
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float3 linear3(float3 a, float3 b, float3 t) => (a + (b-a)/4*t)/6*t*t*t;
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float4 linear3(float4 a, float4 b, float t) => (a + (b-a)/4*t)/6*t*t*t;
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float4 linear3(float4 a, float4 b, float4 t) => (a + (b-a)/4*t)/6*t*t*t;

      #endregion

      #region quadric

      //   ∫f_ab_bc(t)
      // = ∫(f_ab(t) + (f_bc(t) - f_ab(t)*t))
      // = ∫f_ab(t) + ∫((f_bc(t) - f_ab(t))*t)
      // = ∫f_ab(t) + ∫(t*f_bc(t)) - ∫(t*f_ab(t)
      // = ∫f_ab(t) + (t*∫f_bc(t) - ∫∫f_bc(t)) - (t*∫f_ab(t) - ∫∫f_ab(t))
      // = ∫f_ab(t) + t*∫f_bc(t) - ∫∫f_bc(t) - t*∫f_ab(t) + ∫∫f_ab(t)
      // = ∫f_ab(t) - t*∫f_ab(t) + t*∫f_bc(t) - ∫∫f_bc(t) + ∫∫f_ab(t)
      // = (1-t)*∫f_ab(t) + t*∫f_bc(t) + ∫∫f_ab(t) - ∫∫f_bc(t)
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float quadric(float a, float b, float c, float t) => (1-t)*linear(a, b, t) + t*linear(b, c, t) + linear2(a, b, t) - linear2(b, c, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float2 quadric(float2 a, float2 b, float2 c, float t) => (1-t)*linear(a, b, t) + t*linear(b, c, t) + linear2(a, b, t) - linear2(b, c, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float2 quadric(float2 a, float2 b, float2 c, float2 t) => (1-t)*linear(a, b, t) + t*linear(b, c, t) + linear2(a, b, t) - linear2(b, c, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float3 quadric(float3 a, float3 b, float3 c, float t) => (1-t)*linear(a, b, t) + t*linear(b, c, t) + linear2(a, b, t) - linear2(b, c, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float3 quadric(float3 a, float3 b, float3 c, float3 t) => (1-t)*linear(a, b, t) + t*linear(b, c, t) + linear2(a, b, t) - linear2(b, c, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float4 quadric(float4 a, float4 b, float4 c, float t) => (1-t)*linear(a, b, t) + t*linear(b, c, t) + linear2(a, b, t) - linear2(b, c, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float4 quadric(float4 a, float4 b, float4 c, float4 t) => (1-t)*linear(a, b, t) + t*linear(b, c, t) + linear2(a, b, t) - linear2(b, c, t);


      //   ∫∫f_ab_bc(t)
      //  = ∫((1-t)*∫f_ab(t) + t*∫f_bc(t) + ∫∫f_ab(t) - ∫∫f_bc(t))
      //  = ∫((1-t)*∫f_ab(t)) + ∫(t*∫f_bc(t)) + ∫∫∫f_ab(t) - ∫∫∫f_bc(t))
      //  = (1-t)*∫∫f_ab(t) + ∫∫∫f_ab(t) + t*∫∫f_bc(t)) - ∫∫∫f_bc(t) + ∫∫∫f_ab(t) - ∫∫∫f_bc(t))
      //  = (1-t)*∫∫f_ab(t) + t*∫∫f_bc(t)) 2*(+ ∫∫∫f_ab(t)) - ∫∫∫f_bc(t))
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float quadric2(float a, float b, float c, float t) => (1-t)*linear2(a, b, t) + t*linear2(b, c, t) + 2*(linear3(a,b, t) - linear3(b,c, t));
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float2 quadric2(float2 a, float2 b, float2 c, float t) => (1-t)*linear2(a, b, t) + t*linear2(b, c, t) + 2*(linear3(a,b, t) - linear3(b,c, t));
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float2 quadric2(float2 a, float2 b, float2 c, float2 t) => (1-t)*linear2(a, b, t) + t*linear2(b, c, t) + 2*(linear3(a,b, t) - linear3(b,c, t));
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float3 quadric2(float3 a, float3 b, float3 c, float t) => (1-t)*linear2(a, b, t) + t*linear2(b, c, t) + 2*(linear3(a,b, t) - linear3(b,c, t));
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float3 quadric2(float3 a, float3 b, float3 c, float3 t) => (1-t)*linear2(a, b, t) + t*linear2(b, c, t) + 2*(linear3(a,b, t) - linear3(b,c, t));
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float4 quadric2(float4 a, float4 b, float4 c, float t) => (1-t)*linear2(a, b, t) + t*linear2(b, c, t) + 2*(linear3(a,b, t) - linear3(b,c, t));
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float4 quadric2(float4 a, float4 b, float4 c, float4 t) => (1-t)*linear2(a, b, t) + t*linear2(b, c, t) + 2*(linear3(a,b, t) - linear3(b,c, t));

      #endregion

      #region cubic

      // ∫f_abc_bcd(t) = ∫(f_abc(t) + (f_bcd(t) - f_abc(t))*t)
      //               = ∫f_abc(t) + ∫((f_bcd(t) - f_abc(t))*t)
      //               = ∫f_abc(t) + t*∫(f_bcd(t) - f_abc(t)) - ∫∫(f_bcd(t) - f_abc(t))
      //               = ∫f_abc(t) + t*∫f_bcd(t) - t*f_abc(t) - ∫∫(f_bcd(t) - f_abc(t))
      //               = (1-t)*∫f_abc(t) + t*∫f_bcd(t) - ∫∫(f_bcd(t) - f_abc(t))
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float cubic(float a, float b, float c, float d, float t) => (1-t)*quadric(a, b, c, t) + t*quadric(b, c, d, t) + quadric2(a, b, c, t) - quadric2(b, c, d, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float2 cubic(float2 a, float2 b, float2 c, float2 d, float t) => (1-t)*quadric(a, b, c, t) + t*quadric(b, c, d, t) + quadric2(a, b, c, t) - quadric2(b, c, d, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float2 cubic(float2 a, float2 b, float2 c, float2 d, float2 t) => (1-t)*quadric(a, b, c, t) + t*quadric(b, c, d, t) + quadric2(a, b, c, t) - quadric2(b, c, d, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float3 cubic(float3 a, float3 b, float3 c, float3 d, float t) => (1-t)*quadric(a, b, c, t) + t*quadric(b, c, d, t) + quadric2(a, b, c, t) - quadric2(b, c, d, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float3 cubic(float3 a, float3 b, float3 c, float3 d, float3 t) => (1-t)*quadric(a, b, c, t) + t*quadric(b, c, d, t) + quadric2(a, b, c, t) - quadric2(b, c, d, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float4 cubic(float4 a, float4 b, float4 c, float4 d, float t) => (1-t)*quadric(a, b, c, t) + t*quadric(b, c, d, t) + quadric2(a, b, c, t) - quadric2(b, c, d, t);
      [MethodImpl(MethodImplOptions.AggressiveInlining)]
      public static float4 cubic(float4 a, float4 b, float4 c, float4 d, float4 t) => (1-t)*quadric(a, b, c, t) + t*quadric(b, c, d, t) + quadric2(a, b, c, t) - quadric2(b, c, d, t);

      #endregion

    }

    #endregion

  }

}
