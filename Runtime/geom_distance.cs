using System.Runtime.CompilerServices;
using Unity.Mathematics;

namespace Monoid.Unity.Mathematics {

  unsafe public static partial class geom {

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float distance(in circle c, float2 p) => rad_distance(c.radius, p - c.center);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float sdistance(in circle c, float2 p) => rad_sdistance(c.radius, p - c.center);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    static float rad_distance(float r, float2 p) => math.max(0, rad_sdistance(r, p));

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    static float rad_sdistance(float r, float2 p) => math.length(p) - r;

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float distance(in aabox2 b, float2 p) => ext_distance(b.extents, p - b.center);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static float sdistance(in aabox2 b, float2 p) => ext_sdistance(b.extents, p - b.center);

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    static float ext_distance(float2 e, float2 p) => math.max(0, ext_sdistance(e, p));

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    static float ext_sdistance(float2 e, float2 p) {
      var d = math.abs(p) - e;
      return math.length(math.max(0, d)) + math.min(math.cmax(d), 0);
    }
  }

}
