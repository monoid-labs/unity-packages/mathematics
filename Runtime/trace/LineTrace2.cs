using Unity.Mathematics;

namespace Monoid.Unity.Mathematics.Trace {

  public struct LineTrace2 {

    public struct Step {
      public int2 direction;
      public float time;
    }

    public float2 time, delta;
    public int2 direction;


    public static LineTrace2 New(float2 start, float2 direction) =>
      New(start, direction, float2.zero);

    public static LineTrace2 New(float2 start, float2 direction, float2 center) {
      var trace = new LineTrace2();
      var step = math.sign(direction);
      if (step.x == 0) {
        step.x = 1.0f;
      }
      if (step.y == 0) {
        step.y = 1.0f;
      }

      var p = math.round(start) + step * (0.5f + center);                // 2 lines to intersect with (also their intersection)
      var x = start - p;                                                 // difference in line origins
      var dd = math.float2(1) / direction;                               // displacement
      trace.time = math.abs(x * dd);                                     // line parameter t at plane intersections
      trace.delta = math.abs(step * dd);                                 // change in t after step
      trace.direction = math.int2(step);
      return trace;
    }

    public Step Trace4() {
      var step = new Step();
      // step along axis with the least non-zero error
      if (this.direction.x != 0 && (this.direction.y == 0 || this.time.x < this.time.y)) {
        step.time = this.time.x;
        this.time.x += this.delta.x;
        step.direction.x = this.direction.x;
        return step;
      }
      if (this.direction.y != 0) {
        step.time = this.time.y;
        this.time.y += this.delta.y;
        step.direction.y = this.direction.y;
        return step;
      }
      return step;
    }

    public Step Trace8(float eps = 0.0f) {
      var step = new Step();
      // step along axis with the least non-zero error
      if (this.direction.x != 0 && this.direction.y != 0 && math.abs(this.time.x - this.time.y) <= eps) {
        step.time = math.min(this.time.x, this.time.y);
        this.time += this.delta;
        step.direction = this.direction;
        return step;
      }
      if (this.direction.x != 0 && (this.direction.y == 0 || this.time.x < this.time.y)) {
        step.time = this.time.x;
        this.time.x += this.delta.x;
        step.direction.x = this.direction.x;
        return step;
      }
      if (this.direction.y != 0) {
        step.time = this.time.y;
        this.time.y += this.delta.y;
        step.direction.y = this.direction.y;
        return step;
      }
      return step;
    }

  }

}
