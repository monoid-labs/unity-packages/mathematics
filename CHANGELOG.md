# Changelog

All notable changes to this package will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.0.7-preview] - 2021-01-22

- line2 casting

## [0.0.7-preview] - 2021-01-01

- line2 & intersection

## [0.0.6-preview] - 2020-12-27

- bezier sampling and integrals
- alpha/interpolators (smooth start/step/stop ..)

## [0.0.5-preview] - 2020-12-04

- circle-circle overlap test

## [0.0.4-preview] - 2020-07-18

- bounds for shapes
- geom(etric) tools in separate files

## [0.0.3-preview] - 2020-02-24

- BSD 2-Clause License
- `.editorconfig`

## [0.0.2-preview] - 2020-02-24

2D Line Tracing.

## [0.0.1-preview] - 2020-01-07

This is the first release.
