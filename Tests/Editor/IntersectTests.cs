using UnityEngine; // Mathf.Approximately(expected, actual)
using Unity.Mathematics;
using NUnit.Framework;

namespace Monoid.Unity.Mathematics {

  public static class IntersectTests {

    [Test]
    public static void TestIntersectLine2() {
      {
        var a = new line2 { from = math.float2(0, 0), to = math.float2(10, 0) };
        var b = new line2 { from = math.float2(5, 2), to = math.float2(5, -6) };
        TestIntersectLine2(true, math.float2(0.5f, 0.25f), in a, in b);
      }
      {
        var a = new line2 { from = math.float2(-50, -50), to = math.float2(-50, +50) };
        var b = new line2 { from = math.float2(-49, +20), to = math.float2(-51, +20) };
        TestIntersectLine2(true, math.float2(0.7f, 0.5f), in a, in b);
      }

      /*static*/ void TestIntersectLine2(bool hit, float2 t, in line2 a, in line2 b) {
        bool actual_hit = geom.intersect(in a, in b, out var actual_t);
        Assert.AreEqual(hit, actual_hit, $"intersect({a},{b}) -> {actual_hit} != {hit}");
        if (hit && hit == actual_hit) {
          Assert.True(Mathf.Approximately(t.x, actual_t.x) && Mathf.Approximately(t.y, actual_t.y),
                      $"intersect({a},{b}) -> {actual_t} != {t}");
        }
      }
    }


  }
}
