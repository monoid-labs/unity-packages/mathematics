using Unity.Mathematics;
using NUnit.Framework;

namespace Monoid.Unity.Mathematics {

  public static class CircleTests {

    [Test]
    public static void TestSignedDistanceToPoint() {
      var circle = new circle { center = math.float2(1, 2), radius = 1 };

      TestSignedDistanceToPoint(math.length(circle.center) - circle.radius, circle, float2.zero);
      TestSignedDistanceToPoint(-circle.radius, circle, circle.center);
    }

    static void TestSignedDistanceToPoint(float expected, circle circle, float2 point) {
      var actual = geom.sdistance(in circle, point);
      Assert.AreEqual(expected, actual, $"Circle({circle}.SignedDistanceToPoint({point})");
    }

  }
}