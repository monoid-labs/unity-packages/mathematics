using System;
using UnityEngine; // Mathf.Approximately(expected, actual)
using Unity.Mathematics;
using NUnit.Framework;

namespace Monoid.Unity.Mathematics {

  public static partial class Bezier {

    [Test]
    public static void TestLinear() {

      // linear(a,b, t) = a - a*t + b*t

      float[] samples = { 0, 1, 0.25f, 0.5f, 0.75f };
      {  // linear(a=0, b=0, t) = 0
        float a = 0, b = 0;
        TestLinearSamples((t) => 0, a,b, samples);
      }
      {  // linear(a=0, b=1, t) = t
        float a = 0, b = 1;
        TestLinearSamples((t) => t, a,b, samples);
      }
      {  // linear(a=1, b=0, t) = 1-t
        float a = 1, b = 0;
        TestLinearSamples((t) => 1-t, a,b, samples);
      }
      {  // linear(a=1, b=1, t) = 1
        float a = 1, b = 1;
        TestLinearSamples((t) => 1, a,b, samples);
      }

      /*static*/ void TestLinearSamples(Func<float, float> expr, float a, float b, float[] times) {
        foreach(var t in times) {
          TestLinear(expr(t), a, b, t);
        }
      }
      /*static*/ void TestLinear(float expected, float a, float b, float t) {
        float actual = bezier.linear(a, b, t);
        Assert.True(Mathf.Approximately(expected, actual), $"linear({a},{b}, {t}) -> {actual} != {expected}");
      }
    }

    [Test]
    public static void TestQuadric() {

      // quadric(a,b,c t) = a*t^2 - 2*a*t + a - 2*b*t^2 + 2*b*t + c*t^2

      float[] samples = { 0, 1, 0.25f, 0.5f, 0.75f };

      { // quadric(a=0, b=0, c=0, t) = 0
        float a = 0, b = 0, c = 0;
        TestQuadricSamples((t) => 0, a,b,c, samples);
      }
      { // quadric(a=0, b=0, c=1, t) = t^2
        float a = 0, b = 0, c = 1;
        TestQuadricSamples((t) => t*t, a,b,c, samples);
      }
      { // quadric(a=0, b=1, c=1, t) = -t^2 - 2*t = (2-t)*t
        float a = 0, b = 1, c = 1;
        TestQuadricSamples((t) => (2-t)*t, a,b,c, samples);
      }
      { // quadric(a=1, b=0, c=0, t) = 1 - 2*t + t^2 = 1 + (t-2)*t
        float a = 1, b = 0, c = 0;
        TestQuadricSamples((t) => 1 + (t-2)*t, a,b,c, samples);
      }
      { // quadric(a=1, b=1, c=1, t) = 1
        float a = 1, b = 1, c = 1;
        TestQuadricSamples((t) => 1, a,b,c, samples);
      }

      /*static*/ void TestQuadricSamples(Func<float, float> expr, float a, float b, float c, float[] times) {
        foreach(var t in times) {
          TestQuadric(expr(t), a, b, c, t);
        }
      }
      /*static*/ void TestQuadric(float expected, float a, float b, float c, float t) {
        float actual = bezier.quadric(a, b, c, t);
        Assert.True(Mathf.Approximately(expected, actual), $"quadric({a},{b},{c}, {t}) => {actual} != {expected}");
      }
    }


    [Test]
    public static void TestCubic() {

      // cubic(a,b,c t) = -a*t^3 + 3*a*t^2 - 3*a*t + a + 3*b*t^3 - 6*b*t^2 + 3*b*t - 3*c*t^3 + 3*c*t^2 + d*t^3

      float[] samples = { 0, 1, 0.25f, 0.5f, 0.75f };

      { // cubic(a=0, b=0, c=0, d=0, t) = 0
        float a = 0, b = 0, c = 0, d = 0;
        TestCubicSamples((t) => 0, a,b,c,d, samples);
      }
      { // cubic(a=0, b=0, c=0, d=1, t) = t^3
        float a = 0, b = 0, c = 0, d = 1;
        TestCubicSamples((t) => t*t*t, a,b,c,d, samples);
      }
      { // cubic(a=0, b=0, c=1, d=1, t) = -2*t^3 + 3*t^2 = (3 - 2*t)*t^2
        float a = 0, b = 0, c = 1, d = 1;
        TestCubicSamples((t) => (3 - 2*t)*t*t, a,b,c,d, samples);
      }
      { // cubic(a=0, b=1, c=1, d=1, t) = t^3 - 3*t^2 + 3*t = ((t - 3)*t + 3)*t
        float a = 0, b = 1, c = 1, d = 1;
        TestCubicSamples((t) => ((t - 3)*t + 3)*t, a,b,c,d, samples);
      }
      { // cubic(a=1, b=1, c=1, d=1, t) = 1
        float a = 1, b = 1, c = 1, d = 1;
        TestCubicSamples((t) => 1, a,b,c,d, samples);
      }

      /*static*/ void TestCubicSamples(Func<float, float> expr, float a, float b, float c, float d, float[] times) {
        foreach(var t in times) {
          TestCubic(expr(t), a, b, c, d, t);
        }
      }
      /*static*/ void TestCubic(float expected, float a, float b, float c, float d, float t) {
        float actual = bezier.cubic(a, b, c, d, t);
        Assert.True(Mathf.Approximately(expected, actual), $"cubic({a},{b},{c},{d}, {t}) => {actual} != {expected}");
      }
    }

    [Test]
    public static void TestLinearIntegral() {

      // ∫linear(a,b, t) = ∫(a - a*t + b*t) = ∫a - a*∫t + b*∫t = a*t - a/2*t^2 + b/2*t^2

      float[] samples = { 0, 1, 0.25f, 0.5f, 0.75f };
      {  // ∫linear(a=0, b=0, t) = 0
        float a = 0, b = 0;
        TestLinearSamples((t) => 0, a,b, samples);
      }
      {  // ∫linear(a=0, b=1, t) = t^2/2
        float a = 0, b = 1;
        TestLinearSamples((t) => t*t/2, a,b, samples);
      }
      {  // ∫linear(a=1, b=0, t) = t - t^2/2 = (1-t/2)*t
        float a = 1, b = 0;
        TestLinearSamples((t) => (1-t/2)*t, a,b, samples);
      }
      {  // ∫linear(a=1, b=1, t) = t - t^2/2 + t^2/2 = t
        float a = 1, b = 1;
        TestLinearSamples((t) => t, a,b, samples);
      }

      /*static*/ void TestLinearSamples(Func<float, float> expr, float a, float b, float[] times) {
        foreach(var t in times) {
          TestLinear(expr(t), a, b, t);
        }
      }
      /*static*/ void TestLinear(float expected, float a, float b, float t) {
        float actual = bezier.integral.linear(a, b, t);
        Assert.True(Mathf.Approximately(expected, actual), $"∫linear({a},{b}, {t}) -> {actual} != {expected}");
      }
    }

    [Test]
    public static void TestLinearIntegral2() {

      // ∫∫linear(a,b, t) = ∫(a*t - a/2*t^2 + b/2*t^2) = a*∫t - a/2*∫t^2 + b/2*∫t^2
      //                                               = a/2*t^2 - a/6*t^3 + b/6*t^3

      float[] samples = { 0, 1, 0.25f, 0.5f, 0.75f };
      {  // ∫∫linear(a=0, b=0, t) = 0
        float a = 0, b = 0;
        TestLinear2Samples((t) => 0, a,b, samples);
      }
      {  // ∫∫linear(a=0, b=1, t) = t^3/6
        float a = 0, b = 1;
        TestLinear2Samples((t) => t*t*t/6, a,b, samples);
      }
      {  // ∫∫linear(a=1, b=0, t) = t^2/2 - t^3/6 = (1-t/3)*t^2/2
        float a = 1, b = 0;
        TestLinear2Samples((t) => (1-t/3)*t*t/2, a,b, samples);
      }
      {  // ∫∫linear(a=1, b=1, t) = t^2/2 - t^3/6 + t^3/6 = t^2/2
        float a = 1, b = 1;
        TestLinear2Samples((t) => t*t/2, a,b, samples);
      }

      /*static*/ void TestLinear2Samples(Func<float, float> expr, float a, float b, float[] times) {
        foreach(var t in times) {
          TestLinear2(expr(t), a, b, t);
        }
      }
      /*static*/ void TestLinear2(float expected, float a, float b, float t) {
        float actual = bezier.integral.linear2(a, b, t);
        Assert.True(Mathf.Approximately(expected, actual), $"∫∫linear({a},{b}, {t}) -> {actual} != {expected}");
      }
    }


    [Test]
    public static void TestLinearIntegral3() {

      // ∫∫∫linear(a,b, t) = ∫(a/2*t^2 - a/6*t^3 + b/6*t^3) = a/2*∫t^2 - a/6*∫t^3 + b/6*∫t^3
      //                                                    = a/6*t^3 - a/24*t^4 + b/24*t^4

      float[] samples = { 0, 1, 0.25f, 0.5f, 0.75f };
      {  // ∫∫∫linear(a=0, b=0, t) = 0
        float a = 0, b = 0;
        TestLinear3Samples((t) => 0, a,b, samples);
      }
      {  // ∫∫∫linear(a=0, b=1, t) = t^4/24
        float a = 0, b = 1;
        TestLinear3Samples((t) => t*t*t*t/24, a,b, samples);
      }
      {  // ∫∫∫linear(a=1, b=0, t) = t^3/6 - t^4/24 = (1-t/4)*t^3/6
        float a = 1, b = 0;
        TestLinear3Samples((t) => (1-t/4)*t*t*t/6, a,b, samples);
      }
      {  // ∫∫∫linear(a=1, b=1, t) = t^3/6 - t^4/24 + t^4/24 = t^3/6
        float a = 1, b = 1;
        TestLinear3Samples((t) => t*t*t/6, a,b, samples);
      }

      /*static*/ void TestLinear3Samples(Func<float, float> expr, float a, float b, float[] times) {
        foreach(var t in times) {
          TestLinear3(expr(t), a, b, t);
        }
      }
      /*static*/ void TestLinear3(float expected, float a, float b, float t) {
        float actual = bezier.integral.linear3(a, b, t);
        Assert.True(Mathf.Approximately(expected, actual), $"∫∫∫linear({a},{b}, {t}) -> {actual} != {expected}");
      }
    }

    [Test]
    public static void TestQuadricIntegral() {

      // ∫quadric(a,b,c t) = ∫(a*t^2 - 2*a*t + a - 2*b*t^2 + 2*b*t + c*t^2)
      //                   = a*∫t^2 - 2*a*∫t + a*∫1 - 2*b*∫t^2 + 2*b*∫t + c*∫t^2)
      //                   = a/3*t^3 - a*t^2 + a*t - 2/3*b*t^3 + b*t^2 + c/3*t^3

      float[] samples = { 0, 1, 0.25f, 0.5f, 0.75f };
      { // ∫quadric(a=0, b=0, c=0, t) = 0
        float a = 0, b = 0, c = 0;
        TestQuadricSamples((t) => 0, a,b,c, samples);
      }
      { // ∫quadric(a=0, b=0, c=1, t) = t^3/3
        float a = 0, b = 0, c = 1;
        TestQuadricSamples((t) => t*t*t/3, a,b,c, samples);
      }
      { // ∫quadric(a=0, b=1, c=1, t) = -2/3*t^3 + t^2 + t^3/3 = -t^3/3 + t^2 = (1-t/3)*t^2
        float a = 0, b = 1, c = 1;
        TestQuadricSamples((t) => (1-t/3)*t*t, a,b,c, samples);
      }
      { // ∫quadric(a=1, b=0, c=0, t) = t^3/3 - t^2 + t  = (1 + (t/3 - 1)*t)*t
        float a = 1, b = 0, c = 0;
        TestQuadricSamples((t) => (1 + (t/3-1)*t)*t, a,b,c, samples);
      }
      { // ∫quadric(a=1, b=1, c=1, t) = t^3/3 - t^2 + t - t^3/3 + t^2 = t
        float a = 1, b = 1, c = 1;
        TestQuadricSamples((t) => t, a,b,c, samples);
      }

      /*static*/ void TestQuadricSamples(Func<float, float> expr, float a, float b, float c, float[] times) {
        foreach(var t in times) {
          TestQuadric(expr(t), a, b, c, t);
        }
      }
      /*static*/ void TestQuadric(float expected, float a, float b, float c, float t) {
        float actual = bezier.integral.quadric(a, b, c, t);
        Assert.True(Mathf.Approximately(expected, actual), $"∫quadric({a},{b},{c}, {t}) => {actual} != {expected}");
      }
    }

    [Test]
    public static void TestQuadricIntegral2() {

      // ∫∫quadric(a,b,c t) = ∫(a/3*t^3 - a*t^2 + a*t - 2/3*b*t^3 + b*t^2 + c/3*t^3)
      //                    = a/3*∫t^3 - a*∫t^2 + a*∫t - 2/3*b*∫t^3 + b*∫t^2 + c/3*∫t^3
      //                    = a/12*t^4 - a/3*t^3 + a/2*t^2 - b/6*t^4 + b/3*t^3 + c/12*t^4

      float[] samples = { 0, 1, 0.25f, 0.5f, 0.75f };
      { // ∫∫quadric(a=0, b=0, c=0, t) = 0
        float a = 0, b = 0, c = 0;
        TestQuadric2Samples((t) => 0, a,b,c, samples);
      }
      { // ∫∫quadric(a=0, b=0, c=1, t) = t^4/12
        float a = 0, b = 0, c = 1;
        TestQuadric2Samples((t) => t*t*t*t/12, a,b,c, samples);
      }
      { // ∫∫quadric(a=0, b=1, c=1, t) = -t^4/6 + t^3/3 + t^4/12 = t^3/3 - t^4/12 = (1-t/4)*t^3/3
        float a = 0, b = 1, c = 1;
        TestQuadric2Samples((t) => (1-t/4)*t*t*t/3, a,b,c, samples);
      }
      { // ∫∫quadric(a=1, b=0, c=0, t) = t^4/12 - t^3/3 + t^2/2 = (t^2/12 - t/3 + 1/2)*t^2 = ((t/12 - 1/3)*t + 1/2)*t^2
        float a = 1, b = 0, c = 0;
        TestQuadric2Samples((t) => ((t/12 - 1.0f/3.0f)*t + 0.5f)*t*t, a,b,c, samples);
      }
      { // ∫∫quadric(a=1, b=1, c=1, t) = t^4/12 - t^3/3 + t^2/2 - t^4/6 + t^3/3 + t^4/12 = t^2/2
        float a = 1, b = 1, c = 1;
        TestQuadric2Samples((t) => t*t/2, a,b,c, samples);
      }

      /*static*/ void TestQuadric2Samples(Func<float, float> expr, float a, float b, float c, float[] times) {
        foreach(var t in times) {
          TestQuadric2(expr(t), a, b, c, t);
        }
      }
      /*static*/ void TestQuadric2(float expected, float a, float b, float c, float t) {
        float actual = bezier.integral.quadric2(a, b, c, t);
        Assert.True(Mathf.Approximately(expected, actual), $"∫∫quadric({a},{b},{c}, {t}) => {actual} != {expected}");
      }
    }


    [Test]
    public static void TestCubicIntegral() {

      // ∫cubic(a,b,c,d t) = ∫(-a*t^3 + 3*a*t^2 - 3*a*t + a + 3*b*t^3 - 6*b*t^2 + 3*b*t - 3*c*t^3 + 3*c*t^2 + d*t^3)
      //                   = -a*∫t^3 + 3*a*∫t^2 - 3*a*∫t + a*∫1 + 3*b*∫t^3 - 6*b*∫t^2 + 3*b*∫t - 3*c*∫t^3 + 3*c*∫t^2 + d*∫t^3
      //                   = -a/4*t^4 + a*t^3 - 3/2*a*t^2 + a*t + 3/4*b*t^4 - 2*b*t^3 + 3/2*b*t^2 - 3/4*c*t^4 + c*t^3 + d/4*t^4

      float[] samples = { 0, 1, 0.25f, 0.5f, 0.75f };
      { // ∫cubic(a=0, b=0, c=0, d=0, t) = 0
        float a = 0, b = 0, c = 0, d = 0;
        TestCubicSamples((t) => 0, a,b,c,d, samples);
      }
      { // ∫cubic(a=0, b=0, c=0, d=1, t) = t^4/4
        float a = 0, b = 0, c = 0, d = 1;
        TestCubicSamples((t) => t*t*t*t/4, a,b,c,d, samples);
      }
      { // ∫cubic(a=0, b=0, c=1, d=1, t) = - 3/4*t^4 + t^3 + t^4/4 = t^3 - t^4/2 = (1-t/2)*t^3
        float a = 0, b = 0, c = 1, d = 1;
        TestCubicSamples((t) => (1-t/2)*t*t*t, a,b,c,d, samples);
      }
      { // ∫cubic(a=0, b=1, c=1, d=1, t) = 3/4*t^4 - 2*t^3 + 3/2*t^2 - 3/4*t^4 + t^3 + t^4/4 = t^4/4 - t^3 + 3/2*t^2
        //                               = (t^2/4 - t + 3/2)*t^2 = ((t/4 - 1)*t + 3/2)*t^2
        float a = 0, b = 1, c = 1, d = 1;
        TestCubicSamples((t) => ((t/4-1)*t + 1.5f)*t*t, a,b,c,d, samples);
      }
      { // ∫cubic(a=1, b=1, c=1, d=1, t) = -t^4/4 + t^3 - 3/2*t^2 + t + 3/4*t^4 - 2*t^3 + 3/2*t^2 - 3/4*t^4 + t^3 + t^4/4 = t
        float a = 1, b = 1, c = 1, d = 1;
        TestCubicSamples((t) => t, a,b,c,d, samples);
      }

      /*static*/ void TestCubicSamples(Func<float, float> expr, float a, float b, float c, float d, float[] times) {
        foreach(var t in times) {
          TestCubic(expr(t), a, b, c, d, t);
        }
      }
      /*static*/ void TestCubic(float expected, float a, float b, float c, float d, float t) {
        float actual = bezier.integral.cubic(a, b, c, d, t);
        Assert.True(Mathf.Approximately(expected, actual), $"∫cubic({a},{b},{c},{d}, {t}) => {actual} != {expected}");
      }
    }

  }

}
