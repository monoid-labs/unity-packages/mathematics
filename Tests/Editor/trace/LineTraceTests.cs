using Unity.Mathematics;
using NUnit.Framework;


namespace Monoid.Unity.Mathematics.Trace {

  public static partial class TraceTests {

    [Test]
    public static void LineTraces2D() {
      LineTrace2.Step[] expected;

      // misc
      expected = new LineTrace2.Step[] {
        new LineTrace2.Step { direction = math.int2(1, 1), time = 0.5f },
      };
      CompareResults(expected, PixelTrace8(math.float2(143, 121), math.float2(144, 122), 1));
      CompareResults(expected, PixelTrace8(math.float2(111, 143), math.float2(112, 144), 1));
      expected = new LineTrace2.Step[] {
        new LineTrace2.Step { direction = math.int2(0, 1), time = 0.5f },
      };
      CompareResults(expected, PixelTrace8(math.float2(144, 121), math.float2(144, 122), 1));
      CompareResults(expected, PixelTrace8(math.float2(112, 143), math.float2(112, 144), 1));


      // right
      expected = new LineTrace2.Step[] {
        new LineTrace2.Step { direction = math.int2(1, 0), time = 0.5f / 3 },
        new LineTrace2.Step { direction = math.int2(1, 0), time = 0.5f },
        new LineTrace2.Step { direction = math.int2(1, 0), time = 1 - 0.5f / 3 },
      };
      CompareResults(expected, PixelTrace4(math.float2(0, 0), math.float2(3, 0), 3));
      CompareResults(expected, PixelTrace8(math.float2(0, 0), math.float2(3, 0), 3));
      // up
      expected = new LineTrace2.Step[] {
        new LineTrace2.Step { direction = math.int2(0, 1), time = 0.5f / 3 },
        new LineTrace2.Step { direction = math.int2(0, 1), time = 0.5f },
        new LineTrace2.Step { direction = math.int2(0, 1), time = 1 - 0.5f / 3 },
      };
      CompareResults(expected, PixelTrace4(math.float2(0, 0), math.float2(0, 3), 3));
      CompareResults(expected, PixelTrace8(math.float2(0, 0), math.float2(0, 3), 3));
      // left
      expected = new LineTrace2.Step[] {
        new LineTrace2.Step { direction = math.int2(-1, 0), time = 0.5f / 3 },
        new LineTrace2.Step { direction = math.int2(-1, 0), time = 0.5f },
        new LineTrace2.Step { direction = math.int2(-1, 0), time = 1 - 0.5f / 3 },
      };
      CompareResults(expected, PixelTrace4(math.float2(0, 0), math.float2(-3, 0), 3));
      CompareResults(expected, PixelTrace8(math.float2(0, 0), math.float2(-3, 0), 3));
      // down
      expected = new LineTrace2.Step[] {
        new LineTrace2.Step { direction = math.int2(0, -1), time = 0.5f / 3 },
        new LineTrace2.Step { direction = math.int2(0, -1), time = 0.5f },
        new LineTrace2.Step { direction = math.int2(0, -1), time = 1 - 0.5f / 3 },
      };
      CompareResults(expected, PixelTrace4(math.float2(0, 0), math.float2(0, -3), 3));
      CompareResults(expected, PixelTrace8(math.float2(0, 0), math.float2(0, -3), 3));

      // right-up
      expected = new LineTrace2.Step[] {
        new LineTrace2.Step { direction = math.int2(1, 1), time = 0.5f / 3 },
        new LineTrace2.Step { direction = math.int2(1, 1), time = 0.5f },
        new LineTrace2.Step { direction = math.int2(1, 1), time = 1 - 0.5f / 3 },
      };
      CompareResults(expected, PixelTrace8(math.float2(0, 0), math.float2(3, 3), 3));
      // left-up
      expected = new LineTrace2.Step[] {
        new LineTrace2.Step { direction = math.int2(-1, 1), time = 0.5f / 3 },
        new LineTrace2.Step { direction = math.int2(-1, 1), time = 0.5f },
        new LineTrace2.Step { direction = math.int2(-1, 1), time = 1 - 0.5f / 3 },
      };
      CompareResults(expected, PixelTrace8(math.float2(0, 0), math.float2(-3, 3), 3));
      // left-down
      expected = new LineTrace2.Step[] {
        new LineTrace2.Step { direction = math.int2(-1, -1), time = 0.5f / 3 },
        new LineTrace2.Step { direction = math.int2(-1, -1), time = 0.5f },
        new LineTrace2.Step { direction = math.int2(-1, -1), time = 1 - 0.5f / 3 },
      };
      CompareResults(expected, PixelTrace8(math.float2(0, 0), math.float2(-3, -3), 3));
      // right-down
      expected = new LineTrace2.Step[] {
        new LineTrace2.Step { direction = math.int2(1, -1), time = 0.5f / 3 },
        new LineTrace2.Step { direction = math.int2(1, -1), time = 0.5f },
        new LineTrace2.Step { direction = math.int2(1, -1), time = 1 - 0.5f / 3 },
      };
      CompareResults(expected, PixelTrace8(math.float2(0, 0), math.float2(3, -3), 3));

      // TODO(micha): trace4 diagonal order is undefined so best to test if any order is valid
      // right-up
      expected = new LineTrace2.Step[] {
        new LineTrace2.Step { direction = math.int2(0, 1), time = 0.5f / 3 },
        new LineTrace2.Step { direction = math.int2(1, 0), time = 0.5f / 3 },
        new LineTrace2.Step { direction = math.int2(0, 1), time = 0.5f },
        new LineTrace2.Step { direction = math.int2(1, 0), time = 0.5f },
        new LineTrace2.Step { direction = math.int2(0, 1), time = 1 - 0.5f / 3 },
        new LineTrace2.Step { direction = math.int2(1, 0), time = 1 - 0.5f / 3 },
      };
      CompareResults(expected, PixelTrace4(math.float2(0, 0), math.float2(3, 3), 6));
      // left-up
      expected = new LineTrace2.Step[] {
        new LineTrace2.Step { direction = math.int2(0, 1), time = 0.5f / 3 },
        new LineTrace2.Step { direction = math.int2(-1, 0), time = 0.5f / 3 },
        new LineTrace2.Step { direction = math.int2(0, 1), time = 0.5f },
        new LineTrace2.Step { direction = math.int2(-1, 0), time = 0.5f },
        new LineTrace2.Step { direction = math.int2(0, 1), time = 1 - 0.5f / 3 },
        new LineTrace2.Step { direction = math.int2(-1, 0), time = 1 - 0.5f / 3 },
      };
      CompareResults(expected, PixelTrace4(math.float2(0, 0), math.float2(-3, 3), 6));
      // left-down
      expected = new LineTrace2.Step[] {
        new LineTrace2.Step { direction = math.int2(0, -1), time = 0.5f / 3 },
        new LineTrace2.Step { direction = math.int2(-1, 0), time = 0.5f / 3 },
        new LineTrace2.Step { direction = math.int2(0, -1), time = 0.5f },
        new LineTrace2.Step { direction = math.int2(-1, 0), time = 0.5f },
        new LineTrace2.Step { direction = math.int2(0, -1), time = 1 - 0.5f / 3 },
        new LineTrace2.Step { direction = math.int2(-1, 0), time = 1 - 0.5f / 3 },
      };
      CompareResults(expected, PixelTrace4(math.float2(0, 0), math.float2(-3, -3), 6));
      // right-down
      expected = new LineTrace2.Step[] {
        new LineTrace2.Step { direction = math.int2(0, -1), time = 0.5f / 3 },
        new LineTrace2.Step { direction = math.int2(1, 0), time = 0.5f / 3 },
        new LineTrace2.Step { direction = math.int2(0, -1), time = 0.5f },
        new LineTrace2.Step { direction = math.int2(1, 0), time = 0.5f },
        new LineTrace2.Step { direction = math.int2(0, -1), time = 1 - 0.5f / 3 },
        new LineTrace2.Step { direction = math.int2(1, 0), time = 1 - 0.5f / 3 },
      };
      CompareResults(expected, PixelTrace4(math.float2(0, 0), math.float2(3, -3), 6));

      // off-center, right
      // variant 1
      expected = new LineTrace2.Step[] {
        new LineTrace2.Step { direction = math.int2(1, 0), time = 0.25f },
        new LineTrace2.Step { direction = math.int2(1, 0), time = 1.0f - 0.25f },
      };
      CompareResults(expected, PixelTrace4(math.float2(0, 0.25f), math.float2(2, -0.25f), 2));
      CompareResults(expected, PixelTrace8(math.float2(0, 0.25f), math.float2(2, -0.25f), 2));
      // variant 2
      expected = new LineTrace2.Step[] {
        new LineTrace2.Step { direction = math.int2(1, 0), time = 1 / 6.0f },
        new LineTrace2.Step { direction = math.int2(1, 0), time = 1 - 1 / 6.0f },
      };
      CompareResults(expected, PixelTrace4(math.float2(0.25f, 0), math.float2(1.75f, 0.0f), 2));
      CompareResults(expected, PixelTrace8(math.float2(0.25f, 0), math.float2(1.75f, 0.0f), 2));
      // variant 3
      expected = new LineTrace2.Step[] {
        new LineTrace2.Step { direction = math.int2(1, 0), time = 1 / 6.0f },
        new LineTrace2.Step { direction = math.int2(1, 0), time = 1 - 1 / 6.0f },
      };
      CompareResults(expected, PixelTrace4(math.float2(0.25f, 0.25f), math.float2(1.75f, -0.25f), 2));
      CompareResults(expected, PixelTrace8(math.float2(0.25f, 0.25f), math.float2(1.75f, -0.25f), 2));
    }

    static LineTrace2.Step[] PixelTrace4(float2 from, float2 to, int n) {
      var pixels = LineTrace2.New(from, to - from);
      var actual = new LineTrace2.Step[n];
      for (int i = 0; i < n; ++i) {
        actual[i] = pixels.Trace4();
      }
      return actual;
    }

    static LineTrace2.Step[] PixelTrace8(float2 from, float2 to, int n) {
      var pixels = LineTrace2.New(from, to - from);
      var actual = new LineTrace2.Step[n];
      for (int i = 0; i < n; ++i) {
        actual[i] = pixels.Trace8();
      }
      return actual;
    }

    static void CompareResults(LineTrace2.Step[] expected, LineTrace2.Step[] actual) {
      const float eps = 0.000001f;
      var n = expected.Length;
      if (n != actual.Length) {
        Assert.Fail($"Expected {n} trace steps but was {actual.Length}\n");
        return;
      }
      for (int i = 0; i < n; ++i) {
        if (math.all(expected[i].direction == actual[i].direction) && math.abs(expected[i].time - actual[i].time) <= eps) {
          continue;
        }
        Assert.Fail($"Expected trace step {i} to be {ToString(expected[i])} but was {ToString(actual[i])}\n");
        return;
      }
    }

    static string ToString(in LineTrace2.Step step) {
      return $"{{ direction = {step.direction}, time = {step.time} }}";
    }
  }
}