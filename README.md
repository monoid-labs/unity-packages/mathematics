# Common Math Structures and Functions

All based on the `Unity.Mathematics` package.

## License

BSD 2-Clause License (see [LICENSE](LICENSE.md))
